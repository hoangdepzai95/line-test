const express = require('express');
const bodyParser = require('body-parser');
const api = require('./routes');
const cors = require('cors');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ extended: true }));
app.use(cors());

app.use('/api', api);

app.listen(3000, () => {
    console.log('mock server list on port 3000');
});
