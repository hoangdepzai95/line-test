import React from 'react';
import './line-loading.scss';
import http from '../../services/http';

export default class LineLoading extends React.Component {

    state = {
        waitingForHttpResponse: false
    };

    componentDidMount() {
        http.subscribe(this.onLoadingChange);
    }

    onLoadingChange = (isLoading) => {
        this.setState({
            waitingForHttpResponse: isLoading
        });
    }

    componentWillUnmount() {
        http.unsubscribe(this.onLoadingChange);
    }

    render() {
        const { waitingForHttpResponse } = this.state;

        if (!waitingForHttpResponse) {
            return null;
        }

        return (
            <div className="liner-loading-container">
                <div className="line-loader"></div>
            </div>
        );
    }
}
