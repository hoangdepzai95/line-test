import React from 'react';
import PropTypes from 'prop-types';
import router from '../../services/router';

export default class RouterLink extends React.Component {

    goToLink = ($event) => {
        $event.preventDefault();
        router.navigate(this.props.to);
    }

    render() {
        return (
            <a href={this.props.to} onClick={this.goToLink}>
                {this.props.children}
            </a>
        );
    }
}

RouterLink.propTypes = {
    to: PropTypes.string,
    children: PropTypes.node
};
