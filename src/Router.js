import React from 'react';
import router from './services/router';
import auth from './services/auth';

const DEFAULT_ROUTE = '/';

export default class Router extends React.Component {

    constructor(props) {
        super(props);

        this.routes = [];

        this.state = {
            outletComponent: null
        };
    }

    componentDidMount() {
        this.updateOutlet();
        window.addEventListener('popstate', () => {
            this.updateOutlet();
        });

    }

    updateOutlet() {
        let activeRoute = this.routes.find(route => route.path === window.location.pathname);

        if (!activeRoute) {
            activeRoute = this.routes.find(route => route.path === DEFAULT_ROUTE);
        }

        if (activeRoute.auth && !auth.loggedIn) {
            router.navigate('/login');
        } else {
            this.setState({
                outletComponent: activeRoute.component
            });
        }
    }


    mapRoute(routes) {
        this.routes = routes;
    }

    render() {
        return this.state.outletComponent || null;
    }
}
