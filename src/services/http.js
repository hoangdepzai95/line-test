import { BASE_URL } from '../constant';
import auth from './auth';

const DELAY = 150;

class Http {

    constructor() {
        this.subs = [];
    }

    subscribe(cb) {
        this.subs.push(cb);
    }

    unsubscribe(cb) {
        this.subs = this.subs.filter(sub => sub !== cb);
    }

    sendRequest(method, url, body, params) {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            const finalUrl = this.composeUrl(method, url, params);

            xhr.responseType = 'json';
            xhr.open(method, finalUrl);

            if (finalUrl.includes('/auth/') && auth.loggedIn) {
                xhr.setRequestHeader('Authorization', `Bearer ${auth.getAccessToken()}`);
            }

            xhr.onreadystatechange = () => {
                if(xhr.readyState === 4) {

                    if ([200, 304, 201].includes(xhr.status)) {
                        resolve(xhr.response);
                    } else {
                        reject({ errorCode: xhr.status });
                    }

                    this.emitLoadingState(false);
                }

            };

            setTimeout(() => {
                if ((method === 'PUT' || method === 'POST') && body) {
                    xhr.send(body);
                } else {
                    xhr.send();
                }

                this.emitLoadingState(true);
            }, DELAY);
        });
    }

    emitLoadingState(isLoading) {
        for (let sub of this.subs) {
            if (typeof sub === 'function') {
                sub(isLoading);
            }
        }
    }

    composeUrl(method, url, params) {
        let finalUrl = url;

        if (!url.includes('http://') && !url.includes('https://')) {
            finalUrl = BASE_URL + url;
        }


        if (method === 'GET' && params) {
            finalUrl = `${finalUrl}?${Object.keys(params).map(key => `${key}=${params[key]}`).join('&')}`;
        }

        return finalUrl;
    }

    get(url, params) {
        return this.sendRequest('GET', url, null, params );
    }

    post(url, body) {
        return this.sendRequest('POST', url, body);
    }

    put(url, body) {
        return this.sendRequest('PUT', url, body);
    }

    delete(url) {
        return this.sendRequest('DELETE', url);
    }
}

export default new Http();
