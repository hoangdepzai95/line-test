const ACCESS_TOKEN_KEY = 'access_token';

class Auth {

    getAccessToken() {
        return localStorage.getItem(ACCESS_TOKEN_KEY);
    }

    get loggedIn() {
        return !!this.getAccessToken();
    }
}

export default new Auth();
