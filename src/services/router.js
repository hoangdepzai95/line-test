class Router {

    navigate(path) {
        history.pushState({}, window.title, path);
        window.dispatchEvent(new Event('popstate'));
    }
}

export default new Router();
