import React from 'react';
import Home from './scenes/home';
import LineLoading from 'Component/line-loading';
import Router from './Router';
import Login from './scenes/login';

export default class App extends Router {

    constructor(props) {
        super(props);

        this.mapRoute([
            {
                path: '/login',
                component: <Login/>
            },
            {
                path: '/',
                auth: true,
                component: <Home/>
            }
        ]);

    }

    render() {

        return (
            <React.Fragment>
                <LineLoading></LineLoading>
                { super.render() }
            </React.Fragment>
        );
    }
}
