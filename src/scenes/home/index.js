import React from 'react';
import RouterLink from 'Component/router-link';
import './home.scss';

export default class Home extends React.Component {

    render() {
        return (
            <RouterLink to="/login">
                home
            </RouterLink>
        );
    }
}
